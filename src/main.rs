use chrono::NaiveDate;
use colored::Colorize;
use sscanf::sscanf;
use std::fs;
use std::io::{self, Write};
use std::io::{BufRead, BufReader, Error};

#[derive(Debug)]
struct DailySummary {
    pub date: NaiveDate,
    pub summaries: Vec<Summary>,
}

#[derive(Debug)]
struct Summary {
    pub entry: String,
}

fn date_exists(dailies: &Vec<DailySummary>, date: NaiveDate) -> bool {
    if let Some(_) = dailies.iter().find(|x| x.date == date) {
        return true;
    }

    false
}

fn dailies_pos(dailies: &Vec<DailySummary>, date: NaiveDate) -> Option<usize> {
    dailies.iter().position(|x| x.date == date)
}

fn main() -> Result<(), Error> {
    let file = "work journal.md";
    let mut dailies: Vec<DailySummary> = vec![];
    let buff_reader = BufReader::new(fs::File::open(file)?);
    let mut is_summary = false;
    let mut date = NaiveDate::from_ymd_opt(2000, 1, 1).unwrap();
    for line in buff_reader.lines() {
        let l = line?;
        if let Ok(header) = sscanf!(l, "# {}", String) {
            if let Ok(date_only) = NaiveDate::parse_from_str(&header, "%Y-%m-%d") {
                date = date_only;
            }
        }

        if let Ok(_) = sscanf!(l, "## Summary") {
            is_summary = true;
        } else if let Ok(_) = sscanf!(l, "## {}", String) {
            is_summary = false;
        }

        if is_summary {
            let mut bullet = String::new();
            if let Ok(bul) = sscanf!(l, "* {}", String) {
                bullet = bul;
            }

            if !bullet.is_empty() {
                if !date_exists(&dailies, date) {
                    let mut summaries: Vec<Summary> = vec![];
                    summaries.push(Summary { entry: bullet });
                    dailies.push(DailySummary { date, summaries })
                } else if let Some(i) = dailies_pos(&dailies, date) {
                    dailies[i].summaries.push(Summary { entry: bullet });
                }
            }
        }
    }

    let stdout = io::stdout(); // get the global stdout entity
    let mut handle = stdout.lock(); // acquire a lock on it
    for d in dailies {
        writeln!(
            handle,
            "{} {}",
            d.date.to_string().blue().bold(),
            "summary".blue().bold()
        )?;
        for s in d.summaries {
            writeln!(handle, "• {}", s.entry)?;
        }
        writeln!(handle, "")?;
    }

    Ok(())
}
